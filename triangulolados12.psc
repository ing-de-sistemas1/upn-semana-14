Proceso triangulo
	Definir a,b,c Como Entero
	Escribir "Ingrese los lados del triangulo"
	leer a,b,c
	si a=b y b=c y a=c Entonces
		mostrar "El triangulo es equilatero"
	FinSi
	si a=b y a<>c Entonces
		mostrar "El triangulo es isoceles"
	FinSi
	si a<>b y a<>c y b=c Entonces
		mostrar "El triangulo es isoceles"
	FinSi
	si a<>b y a<>c y b<>c Entonces
		mostrar "El triangulo es escaleno"
	FinSi
	si a=c y b<>c Entonces
		mostrar "El triangulo es isoceles"
	FinSi
FinProceso
